$('.logout').click(
    function () {
        swal({
                title: "Are you sure?",
                text: "Are you sure you want to log out? You will have to sign in with your email address and password the next time you visit!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {

                if (isConfirm) {
                    localStorage.clear();
                    window.location = 'login.html';
                } else {
                    return false;
                }
            });
    }
);