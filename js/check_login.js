token = localStorage.getItem('token');
if (!token) {
    swal('Oops, something went wrong', 'Our tooth fairy think your session may have expired, She will now redirect you back to te log in page', 'error');
    window.location = 'login.html';
}