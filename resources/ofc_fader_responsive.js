    function ofc_fader_responsive(target, duration, transSpeed){

        if ($(target).length){

            // var slide_count = 0;
            // var trans_speed = 500;
            // var time_between = 6000;



            var slide_count = 0;
            var trans_speed = (typeof transSpeed === "undefined") ? 500 : transSpeed;
            var time_between = (typeof duration === "undefined") ? 6000 : duration;

            var target_slide = 2;

            // find how many slides there are
            $(target).children().each(function(){

                slide_count++;

                $(this).addClass('ofc_slide'+slide_count);
                $(this).css('position', 'absolute');
                $(this).hide()
            });

            $(target).find('.ofc_slide1').addClass('front_slide');
            $(target).find('.ofc_slide1').show();

            function fade_timer_exec(control){

                if (control == 'start'){

                    fade_timer = setInterval(function(){

                        bringIn(target_slide);
                    }, time_between);
                }

                else if(control == 'stop'){

                    clearInterval(fade_timer);
                }

            }

            fade_timer_exec('start');

            function bringIn(){

                if (target_slide == slide_count + 1){
                    target_slide = 1;
                }

                // remove old second slide
                $(target).find('.second_slide').removeClass('second_slide');

                // add class to current front slide, remove old front slide class
                $(target).find('.front_slide').addClass('second_slide');
                $(target).find('.second_slide').removeClass('front_slide');

                // add class
                $(target).find('.ofc_slide'+target_slide).addClass('front_slide');

                // fade in
                $(target).find('.front_slide').fadeIn(trans_speed);

                // fade Out
                $(target).find('.second_slide').fadeOut(trans_speed);

                target_slide++;

            }

            var fade_timer;

        }

        // Change height of fader based on height of slides
        $(window).on('resize', function(){
            var biggestHeight = "0";
            // Loop through elements children to find & set the biggest height
            $(target).children().each(function(){
             // If this element's height is bigger than the biggestHeight
             if ($(this).height() > biggestHeight ) {
               // Set the biggestHeight to this Height
               biggestHeight = $(this).height();
             }
            });

            // Set the container height
            $(target).height(biggestHeight);
        }).resize();

    }
