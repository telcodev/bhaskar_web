/**
 * jQuery plugin to add social footer to targetted element
 * 
 *  How to add an ajax overlay:
 *  
 *  $('#socialfooter').append('<div id="appointmentOverlay" class="simple-overlay" style="width:360px;height:450px;"><h1>Request an Appoinment</h1><div class="overlay-content" style="height:425px;"><div id="apptwrap">Loading...</div></div>');
 *  $('#floatingbar ul').append('<li><a id="appointmentTrigger" rel="#appointmentOverlay"  href="/ajaxRenderComponent.html?m=page&c=sbAppointmentForm"><img src="/sbtemplates/sbcommon/images/footer/appointments.png" title="Request an Appointment!" /></a></li>');
 *  $("#appointmentTrigger").overlay({
 *    mask: options.mask, 
 *    onBeforeLoad: function() {
 *      var wrap = this.getOverlay().find(".overlay-content");
 *      wrap.load(this.getTrigger().attr("href"));
 *    }
 *  });
 *  
 *  Example Options object:
 *  options = {
 *        "phone_number":"630-555-5555",
 *        "facebook_domain":"",
 *        "twitter_username":"",
 *        "show":{
 *            "facebook":true,
 *            "plusone":true,
 *            "twitter":true,
 *            "reviews":true,
 *            "appointment":true,
 *            "directions":true,
 *            "locations":true
 *        },
 *        "locations":[{
 *            "name":"Superfab Practice",
 *            "address1":"701 Harger Road",
 *            "address2":"",
 *            "city":"Oak Brook",
 *            "state":"IL",
 *            "zip":"60523",
 *            "country":"USA",
 *            "mapaddress":"701 Harger Road  Oak Brook,IL 60523 USA",
 *            "is_primary":0,
 *            "phone":"630-555-5555"
 *        }]
 *  } 
 * 
 */

(function( $ ){    
   
    var methods = {        
        /**
         * Initialize the toolbar elements
         * 
         * @param object options options object
         * @return this
         */
        init : function( options ) { 
            // Merge options and defaults and save data
            $(this).data('options', $.extend( {
                'twitter_username': 'officite',
                'facebook_domain': 'officite',
                'barheight' : 34,
                'mask' : {
                    color: '#aaa',
                    loadSpeed: 200,
                    opacity: 0.5
                }              
            }, options));                       
            
            var self = $(this);
            
            // Create the bar                        
            var html = '<div id="socialfooter" class="'+options.color_class+'">'+
            '<div id="floatingbar" style="height:'+$(this).data('options').barheight+'px;">'+
            '<ul></ul>'+
            '<div id="showMe"><img src="/sbtemplates/sbcommon/images/footer/show.png" alt="Show"></div>'+                      
            '<div class="rightSide">'+
            '<div id="locTrigger" class="phone" rel="#locOverlay">'+ options.phone_number+'</div>'+
            '<div id="hideMe"><img src="/sbtemplates/sbcommon/images/footer/hide.png" alt="Hide"/></div>'+
            '</div>'+                            
            '</div>'+                         
            '</div>';            
            this.append(html); 
            
            $('#hideMe').click(function() {
                self.sbSocialFooter('hide');
            });        
            
            $('#showMe').click(function(){
                self.sbSocialFooter('show');
            });              
            
            // Check the options we need to initialize
            if(options.show.locations == true && (options.locations.length > 1)){                
                $('#floatingbar').addClass('multiple')
                $(this).sbSocialFooter('insertLocations');
            }            
            if(options.show.facebook == true){
                $(this).sbSocialFooter('insertFacebook');
            }            
            if(options.show.plusone == true){
                $(this).sbSocialFooter('insertPlusone');
            }
            if(options.show.twitter == true){
                $(this).sbSocialFooter('insertTwitter');
            }
            if(options.show.reviews == true){
                $(this).sbSocialFooter('insertReviews');
            }
            if(options.show.appointment == true){
                $(this).sbSocialFooter('insertAppointment');
            }
            if(options.show.directions == true){
                $(this).sbSocialFooter('insertDirections');
            }    
            if(options.show.blog == true){
                $(this).sbSocialFooter('insertBlog');
            } 
            
            
            // hook up the tooltip
            $("#floatingbar img[title]").tooltip();           
            
            // show the bar
            $(this).sbSocialFooter('show');    
            
            return this;
            
        },
        
        /**
         * Add overlay containing locations list
         * @return void
         */
        insertLocations: function(){
            var options = $(this).data('options');
            $('#socialfooter').append('<div id="locOverlay" class="simple-overlay" style="width:505px;height:450px;"><h1>Our Locations</h1><div class="overlay-content" style="height:450px;">Loading...</div></div>'); 
            $("#locTrigger").overlay({
                mask: options.mask,
                onBeforeLoad: function(){                 
                    var wrap = this.getOverlay().find(".overlay-content");
                    wrap.empty();                        
                    for(var i=0; i < options.locations.length; i++){
                        var l = options.locations[i];
                        wrap.append('<div class="footerLocation"><h1>'+options.locations[i].name+'</h1><div class="footerAddress">'+l.address1+' '+ l.address2 +'</div><div class="footerAddress2">'+l.city+' '+ l.state +' '+l.zip+'</div><div class="footerPhone">' + l.phone+'</div></div>');                        
                    }
                }
            });              
        },
        
        /**
         * Add facebook link, like button and overlay
         * @return void
         */
        insertFacebook:function(){
            var options = $(this).data('options');
            if(options.facebook_domain == '' || options.facebook_domain == undefined){  
                return;
            }
            //            $('#socialfooter').append('<div id="facebookOverlay" class="simple-overlay" style="width:300px;height:300px;"><div class="overlay-content"></div></div>')
            //            $('#floatingbar ul').append('<li><a id="facebookTrigger" rel="#facebookOverlay" href="//www.facebook.com/plugins/activity.php?site='+options.facebook_domain+'?iframe=true&width=300&height=300&header=true&colorscheme=light&font&border_color&arecommendations=false"><img src="/sbtemplates/sbcommon/images/footer/facebook.png" title="Check out our Facebook page!"/></a></li>');
            $('#floatingbar ul').append('<li><a id="facebookTrigger" href="//www.facebook.com/'+options.facebook_domain+'" target="_blank"><img src="/sbtemplates/sbcommon/images/footer/facebook.png" title="Check out our Facebook page!" alt="Facebook"/></a></li>');
            $('#floatingbar ul').append('<li class="item"><iframe src="//www.facebook.com/plugins/like.php?app_id=118749601558781&amp;href=facebook.com/'+options.facebook_domain+'&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=25" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:25px;" allowTransparency="true"></iframe></li>');
            
            $("#facebookTrigger").overlay({
                mask: options.mask,
                onBeforeLoad: function(){              
                    var wrap = this.getOverlay().find(".overlay-content");
                    wrap.empty().append('<iframe src="'+this.getTrigger().attr("href")+'" width="300" height="300" frameborder="no"></iframe>');
                }
            });            
        },
        
        /**
         * Add Google Plus One button
         * @return void
         */
        insertPlusone: function(){
            var options = $(this).data('options');
            var self = $(this);            
            //$('#floatingbar ul').append("<li class=\"item\"><g:plusone size=\"medium\" annotation=\"none\"></g:plusone></li>");

            var el = $('#floatingbar ul');

            var li = document.createElement("li");
            li.setAttribute('class', 'item');

                var po = document.createElement('g:plusone');
            po.setAttribute("size", "medium");
            po.setAttribute("annotation", "none");
            li.appendChild(po);

            el.append(li);

            (function() {
                var po = document.createElement('script');
                po.type = 'text/javascript';
                po.async = true;
                po.src = 'https://apis.google.com/js/plusone.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(po, s);
            })();                
        },
        
        /**
         * Add twitter button and overlay with feed content
         * @return void
         */
        insertTwitter:function(){
            var options = $(this).data('options');
            var self = $(this);     
            if(options.twitter_username == '' || options.twitter_username == undefined){  
                return;
            }
//            $('#socialfooter').append('<div id="twitterOverlay" class="simple-overlay" style="width:540px;"><h1>Our Latest Tweets</h1><div class="overlay-content" style="height:425px;"><div class="tweet"></div></div>');
//            $('#floatingbar ul').append('<li><span id="twitterTrigger" rel="#twitterOverlay"><img src="/sbtemplates/sbcommon/images/footer/twitter.png" title="Follow Our Tweets!"/></span></li> ');
//            $("#twitterTrigger").overlay({
//                mask: options.mask,
//                onBeforeLoad: function(){                    
//                    $("#twitterOverlay .overlay-content .tweet").empty().tweet({
//                        join_text: "auto",
//                        username: options.twitter_username,
//                        avatar_size: 48,
//                        count: 5,
//                        auto_join_text_default: "We said,", 
//                        auto_join_text_ed: "We",
//                        auto_join_text_ing: "We were ",
//                        auto_join_text_reply: "We replied",
//                        auto_join_text_url: "we were checking out",
//                        loading_text: "loading tweets..."
//                    });
//                }
//            });             
            $('#floatingbar ul').append('<li><a id="twitterTrigger" href="//twitter.com/'+options.twitter_username+'" target="_blank"><img src="/sbtemplates/sbcommon/images/footer/twitter.png" title="Follow Our Tweets!" alt="Twitter"/></a></li> ');
        },
        
        /**
        * Add link to blog.html
        * @return void
        */
        insertBlog: function(){   
            var options = $(this).data('options');
            var self = $(this);            
            $('#floatingbar ul').append('<li><a id="blogTrigger" href="/blog.html"><img src="/sbtemplates/sbcommon/images/footer/blog.png" title="Visit Our Blog!" alt="Our Blog"/></a></li>');
        },
				
        /**
         * Add link to reviews.html
         * @return void
         */
        insertReviews: function(){   
            var options = $(this).data('options');
            var self = $(this);            
            $('#floatingbar ul').append('<li><a id="reviewTrigger" href="/reviews.html"><img src="/sbtemplates/sbcommon/images/footer/reviews.png" title="Review Us Online!" alt="Reviews Us"/></a></li>');
        },
        
        /**
         * Add link to appointment.html
         * @return void
         */
        insertAppointment: function(){
            var options = $(this).data('options');
            var self = $(this);                 
            $('#floatingbar ul').append('<li><a id="appointmentTrigger" href="/appointment.html"><img src="/sbtemplates/sbcommon/images/footer/appointments.png" title="Request an Appointment!" alt="Request an Appointment"/></a></li>');
        },
        
        /**
         * Add overlay containing Google Map with locations
         * @return void
         */
        insertDirections: function(){
            var options = $(this).data('options');
            var self = $(this);            
            $('#socialfooter').append(' <div id="mapOverlay" class="simple-overlay" style="width:540px;height:450px;"><div class="overlay-content" style="height:450px;">Loading...</div></div>'); 
            $('#floatingbar ul').append('<li><a id="mapTrigger" rel="#mapOverlay" href="#"><img src="/sbtemplates/sbcommon/images/footer/directions.png" title="Get Door-to-door Directions" alt="Directions" /></a></li>');
            $("#mapTrigger").overlay({
                mask: options.mask,                
                onBeforeLoad: function(){                         
//                    var src = new Array();
//                    for(var i=0; i < options.locations.length; i++){
//                        src.push(options.locations[i].mapaddress);
//                    }
//                    src = '//map.officite.com/gmap.php?height=450&width=540&zoom=1&addresses='+src.join('|')+'&num='+options.locations.length;
                    var wrap = this.getOverlay().find(".overlay-content");
                    wrap.empty().append('<iframe src="'+options.frameSource+'" width="540" height="450" frameborder="no" scrolling="no" marginheight="0" marginwidth="0"></iframe>');
                }
            });
        },
        
        /**
         * Show the social footer bar
         * @return void
         */
        show: function(){
            var options = $(this).data('options');            
            // Ease it in
            $('#floatingbar').css({
                height: 0
            }).animate({
                height: options.barheight
            }, 'slow');
            $('#showMe').css({
                height: options.barheight
            }).animate({
                height: '0'
            }, 'slow');      
        },       
       
        /**
        * Hide the social footer bar and show the Show button
        * @return void
        */
        hide : function( ) {
            var options = $(this).data('options');
            $('#floatingbar').css({
                height: options.barheight
            }).animate({
                height: '0'
            }, 'slow');
            $('#showMe').css({
                height: 0
            }).animate({
                height: options.barheight
            }, 'slow');
      
        }
    };
    
    /**
     * Add sbSocialFooter to jQuery
     * 
     * @param string method internal method to call
     * @param object options options to pass to the method
     * @return this;
     */
    $.fn.sbSocialFooter = function(method, options) {                       
        if ( methods[method] ) {
            return methods[method].apply( this, arguments);
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on sbSocialFooter.' );
        } 
    };

   
})( jQuery );

