'use strict';

/**
 * Scripts to handle Testimonials
 */
var AppTestimonials = {
    globalConfig: {
        targetEle: '#testimonial-carousel'
    },
    init: function (config) {
        var me = this;

        // setup configuration.
        me.globalConfig = $.extend({}, me.globalConfig, config);

        var $ele = $(me.globalConfig.targetEle),
                items = $($ele).find('li'),
                index = 0;


    }

};

/**
 * Script to handle funcionality of template.
 */
var AppTemplate = {
    globalConfig: {
        isEditPage: false,
        page: 'index'
    },
    init: function (config) {
        var me = this;

        // setup configuration.
        me.globalConfig = $.extend({}, me.globalConfig, config);

        // Configure scripts per page.
        Ext.onReady(function(){
          me.setupPages(me.globalConfig.page)
        });

        $(window).on('load', function(){
            //me.checkForContactForm();
            window.myTimer = setInterval(me.checkForContactForm, 500);
        });

        $(window).on('resize', function(){
            me.checkForContactForm();
        });

    },
    checkForContactForm: function(){
        if($('#slot-responsive-main-contactus form').length) {
            if(window.innerWidth >= 992) {
                var formHeight = $('#slot-responsive-main-contactus form').outerHeight();
                var commentsMarginTop = parseInt($('#slot-responsive-main-contactus form .form-input-comments').css('marginTop'));
                var commentsLabelHeight = $('#slot-responsive-main-contactus form .form-input-comments .label_container').outerHeight();
                var newCommentsHeight = formHeight - commentsLabelHeight - commentsMarginTop;
            }
            else {
                newCommentsHeight = 150;
            }

            $('#slot-responsive-main-contactus form .form-input-comments .input_container textarea').outerHeight(newCommentsHeight);
            clearInterval(myTimer);
        }
    },
    setupSlider: function () {
        ofc_fader_responsive('#slider');
        $('#mm-slot-navigation').removeClass('header__navigation hidden-xs');
        $('#mm-slot-navigation ul').removeAttr("style");
    },
    animateMenu: function () {
        $("header .nav-primary #slot-navigation").removeClass("menuHidden");
        $(".nav-primary #slot-navigation > ul").append("<span class='stretch'></span>");

        var $el, leftPos, newWidth, rightPos;
        var mainNav = $(".nav-primary #slot-navigation > ul");

        mainNav.append("<span class='magic-line'></span>");

        var waitForLoad = setInterval(function () {
            setTopMenuAnimation();
        }, 100);

        function setTopMenuAnimation() {
            if (mainNav.find('.active').length > 0) {
                for (var i = 0; i < mainNav.length; i++) {
                    var menu = $(mainNav[i]);

                    var magicLine = menu.find(".magic-line");
                    var activeLink = menu.find('.active');

                    magicLine
                            .width(activeLink.children('a').width())
                            .css("left", activeLink.position().left)
                            .data("origLeft", magicLine.position().left)
                            .data("origWidth", magicLine.width());

                    menu.children('li').children('a').hover(function () {
                        $el = $(this);
                        var currentMagicLine = $el.parent().parent().find('.magic-line');

                        leftPos = $el.parent().position().left;
                        newWidth = $el.width();
                        currentMagicLine.stop().animate({
                            left: leftPos,
                            width: newWidth
                        });
                    }, function () {
                        $el = $(this);
                        var currentMagicLine = $el.parent().parent().find('.magic-line');
                        currentMagicLine.stop().animate({
                            left: currentMagicLine.data("origLeft"),
                            width: currentMagicLine.data("origWidth")
                        });
                    });

                    menu.children('li').children('ul').find('a').hover(function () {
                        $el = $(this);
                        var currentMagicLine2 = $el.parent().parent().parent().parent().find('.magic-line');

                        if ($el.parent().parent().parent().is(":last-of-type")) {
                            leftPos = $el.parent().parent().parent().position().left - $el.parent().parent().width() + $el.arent().parent().parent().width() + 12;
                            newWidth = $el.parent().parent().width();
                            currentMagicLine2.stop().animate({
                                left: leftPos,
                                width: newWidth
                            });
                        }
                        else {
                            leftPos = $el.parent().parent().parent().position().left;
                            newWidth = $el.parent().parent().width();
                            currentMagicLine2.stop().animate({
                                left: leftPos,
                                width: newWidth
                            });
                        }
                    }, function () {
                        var currentMagicLine2 = $el.parent().parent().parent().parent().find('.magic-line');
                        currentMagicLine2.stop().animate({
                            left: currentMagicLine2.data("origLeft"),
                            width: currentMagicLine2.data("origWidth")
                        });
                    });
                }

                clearInterval(waitForLoad);
            }
        }

        function stopWaiting() {
            clearInterval(waitForLoad);
        }

    },
    justifyMenu: function () {

        var menus = $(".nav-primary .mlmenu");

        menus.each(function(){

            var Menu = $(this).find(" > ul")
            ,   child   = Menu.children()
            ;

            child.each(function(){
                var temp = $(this);
                $(this).detach();
                Menu.append(temp);
                Menu.append("\n");
            });

        });

        
    },
    setupSidebarMenu: function () {

        if ($('#slot-library ul li.haschild').length !== 0) {

            console.log("child size "+$('#slot-library ul li.haschild').length);

            var menuList = $("#slot-library ul li.haschild");
            //added span tag for the dorop down arrow
            $('#slot-library ul > li.haschild > a').append( "<span></span>" );

            for (var i = 0; i < menuList.length; i++) {
                menuList[i].onmouseover = null;
                menuList[i].onmouseout = null;
            }
            //if ($(window).width() >= 320) {

                $('#slot-library ul > li.haschild > a > span').click(function (e) {
                    $('#slot-library ul > li.haschild > a').not($(this).parents('li').find('a')).each(function () {
                        $(this).next('ul').hide();
                        $(this).parent().removeClass('arrow');
                    });
                    $(this).parent('a').next('ul').show();
                    $(this).parent('a').parent('li.haschild').toggleClass('arrow');
                    e.preventDefault();
                    e.stopPropagation();
                });
            //}

        }
    },
    hideMenuOnEdit: function () {
        var me = this;

        if (me.isEditPage()) {
            $('#se-wrap').hide();
            // $('.sbContainer').css('margin', '0');
        }
    },
    showMenuOnEdit: function () {
        var me = this;
        if (me.isEditPage()) {
            $('#se-wrap').show();
            // $('.sbContainer').css('margin', '40px 0 0 0');
        }
    },
    isEditPage: function () {
        var me = this;
        if (typeof me.globalConfig.isEditPage !== 'undefined' && me.globalConfig.isEditPage === true) {
            return true;
        }

        return false;
    },
    setupPages: function (page) {
        var me = this;

        // Index
        if (page == 'index' || page == '') {

            me.setupIndexView();

        }
        // Contact Us
        else if (page == 'contact') {

            me.setupContactUsView();

        }
        // Staff
        else if (page == 'contact') {

            me.setupStaffView();

        }
        // All other pages.
        else {

            me.setupPageView();

        }

        me.setupForAllPages();

    },
    setupMap: function () {
        //console.log("map");

        var e = $(".js-map-wrapper #mapFrame");
        $(e).attr("style", "pointer-events:none");

        $(".iframeholder").on("click", function (t) {
            t.preventDefault();
            $(e).removeAttr("style")
        });

        $(".iframeholder").mouseleave(function (t) {
            t.preventDefault();
            $(e).attr("style", "pointer-events:none")
        });

        var initialWidth = $(window).width();
        var initialResize = true;

        $(window).on('resize', function () {
            var newWidth = $(window).width();

            if ($('.map-footer #comp-sbMap').size() > 0) {
                if (newWidth != initialWidth || initialResize) {
                    var width = $('.map-footer #comp-sbMap').width();
                    //console.log(width);
                    $('.map-footer #comp-sbMap iframe').attr('width', '100%').attr('height', '550');
                    var heightPattern = /height=[0-9]+/g;
                    var widthPattern = /width=[0-9]+/g;
                    var src = $('.map-footer #comp-sbMap iframe').attr('src')
                            .replace(heightPattern, 'height=550')
                            .replace(widthPattern, 'width=' + width);
                    $('.map-footer #comp-sbMap iframe').attr('src', src);

                    initialResize = false;
                }
            }
        }).trigger('resize');
    },
    setupForAllPages: function () {
        var me = this;

        me.setupMap();

        /* back to top button*/
        $(window).scroll(function () {
            if ($(this).scrollTop() > 400) {
                $('.back_top_link').fadeIn();
            } else {
                $('.back_top_link').fadeOut();
            }
        });

        $('.back_top_link').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
        /* back to top button*/

        me.animateMenu();

        me.justifyMenu();
    },
    setupIndexView: function () {
        var me = this;


        // Setup slider
        me.setupSlider();



        me.setupMap();

        // 
        if (!me.isEditPage()) {

            $('#slot-navigation').mmenu({}, {
                clone: true,
                pageSelector: '.sbContainer',
                menuInjectMethod: 'prepend'
            });

            $('#mm-slot-navigation').removeClass('header__navigation hidden-xs');
            $('#mm-slot-navigation ul').removeAttr('style');
        }

        /*adding active state to the menu*/
        $("#slot-navigation ul li.haschild ul").mouseenter(function () {
            $(this).parent().addClass('menuHover');
        });
        $("#slot-navigation ul li.haschild ul").mouseleave(function () {
            $(this).parent().removeClass('menuHover');
        });
        /*adding active state to the menu*/


        /* back to top button*/
        $(window).scroll(function () {
            if ($(this).scrollTop() > 400) {
                $('.back_top_link').fadeIn();
            } else {
                $('.back_top_link').fadeOut();
            }
        });

        $('.back_top_link').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
        /* back to top button*/



    },
    setupContactUsView: function () {
        var me = this;


        me.setupMap();

        // 
        if (!me.isEditPage()) {

            $('#slot-navigation').mmenu({}, {
                clone: true,
                pageSelector: '.sbContainer',
                menuInjectMethod: 'prepend'
            });

            $('#mm-slot-navigation').removeClass('header__navigation hidden-xs');
            $('#mm-slot-navigation ul').removeAttr('style');
            $('#slot-library').mmenu({}, {clone: true});
            $('#mm-slot-library').removeClass('nav-secondary sidebar-education hidden-xs');
            $('#mm-slot-library ul').removeAttr('style');

        }

        $("#slot-navigation ul li.haschild ul").mouseenter(function () {
            $(this).parent().children('a').addClass('menuHover');
        });
        $("#slot-navigation ul li.haschild ul").mouseleave(function () {
            $(this).parent().children('a').removeClass('menuHover');
        });

        /*patient education side menu made on click instead of hover*/

        removeMouseEvents();
        //setOnClick();

        function removeMouseEvents() {
            var menuList = $("#slot-library ul li.haschild");

            for (var i = 0; i < menuList.length; i++) {
                menuList[i].onmouseover = null;
                menuList[i].onmouseout = null;
            }
        }
        /*
        function setOnClick() {
            $("#slot-library ul li.haschild ul").css('display', 'none');

            $("#slot-library ul li.haschild > a").click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                $(this).siblings('ul').slideToggle();
            });
        }
        ;*/

    },
    setupStaffView: function () {

        var me = this;

        // 
        if (!me.isEditPage()) {

            $('#slot-navigation').mmenu({}, {
                clone: true,
                pageSelector: '.sbContainer',
                menuInjectMethod: 'prepend'
            });
            $('#mm-slot-navigation').removeClass('header__navigation hidden-xs');
            $('#mm-slot-navigation ul').removeAttr('style');
            $('#slot-library').mmenu({}, {clone: true});
            $('#mm-slot-library').removeClass('nav-secondary sidebar-education hidden-xs');
            $('#mm-slot-library ul').removeAttr('style');

        }

        $("#slot-navigation ul li.haschild ul").mouseenter(function () {
            $(this).parent().children('a').addClass('menuHover');
        });
        $("#slot-navigation ul li.haschild ul").mouseleave(function () {
            $(this).parent().children('a').removeClass('menuHover');
        });

        //patient education side menu made on click instead of hover

        removeMouseEvents();
        setOnClick();

        function removeMouseEvents() {
            var menuList = $("#slot-library ul li.haschild");

            for (var i = 0; i < menuList.length; i++) {
                menuList[i].onmouseover = null;
                menuList[i].onmouseout = null;
            }
        }
        /*
        function setOnClick() {
            $("#slot-library ul li.haschild ul").css('display', 'none');

            $("#slot-library ul li.haschild > a").click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                $(this).siblings('ul').slideToggle();
            });
        }
        ;*/
        // back to top button
        $(window).scroll(function () {
            if ($(this).scrollTop() > 400) {
                $('.back_top_link').fadeIn();
            } else {
                $('.back_top_link').fadeOut();
            }
        });
        $('.back_top_link').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
        // back to top button

    },
    setupPageView: function () {

        var me = this;

        me.setupMap();


        // if Ext.onReady hasnt triggered setActiveMenu do it
        if( $('#slot-library ul li.haschild').length == 0){
            mladdevents();
        }

        me.setupSidebarMenu();

        // 
        if (!me.isEditPage()) {

            $('#slot-navigation').mmenu({}, {
                clone: true,
                pageSelector: '.sbContainer',
                menuInjectMethod: 'prepend'
            });
            $('#mm-slot-navigation').removeClass('header__navigation hidden-xs');
            $('#mm-slot-navigation ul').removeAttr('style');
            $('#slot-library').mmenu({}, {clone: true});
            $('#mm-slot-library').removeClass('nav-secondary sidebar-education hidden-xs');
            $('#mm-slot-library ul').removeAttr('style');

        }

        $("#slot-navigation ul li.haschild ul").mouseenter(function () {
            $(this).parent().children('a').addClass('menuHover');
        });
        $("#slot-navigation ul li.haschild ul").mouseleave(function () {
            $(this).parent().children('a').removeClass('menuHover');
        });

        /*patient education side menu made on click instead of hover*/

        removeMouseEvents();
        

        function removeMouseEvents() {
            var menuList = $("#slot-library ul li.haschild");

            for (var i = 0; i < menuList.length; i++) {
                menuList[i].onmouseover = null;
                menuList[i].onmouseout = null;
            }
        }
        
        $("#slot-responsive-search-form-with-button .sidebar-search-field").addClass("searchDefault");


        $("#slot-responsive-search-form-with-button .sidebar-search-field").focus(function () {
            $(this).removeClass("searchDefault");
        });

        $("#slot-responsive-search-form-with-button .sidebar-search-field").blur(function () {
            if ($(this).attr("placeholder") === this.value)
                $(this).addClass("searchDefault");
        });

        /* back to top button*/
        $(window).scroll(function () {
            if ($(this).scrollTop() > 400) {
                $('.back_top_link').fadeIn();
            } else {
                $('.back_top_link').fadeOut();
            }
        });
        $('.back_top_link').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
        /* back to top button*/


    },
    setupBlogView: function () {

        var me = this;

        // 
        if (!me.isEditPage()) {

            $('#slot-navigation').mmenu({}, {clone: true});
            $('#mm-slot-navigation').removeClass('header__navigation hidden-xs');
            $('#mm-slot-navigation ul').removeAttr("style");

        }

    }


};