"use strict";

(function(SMB) {
    var protocol = (location.protocol === 'https:' ? 'https:' : 'http:');

    function markAddress(map, address, bStreet) {
        var parameters = { format: 'json' };

        if (typeof address === 'string') {
            address = address.replace('  ', ' ');
            parameters.q = encodeURIComponent(address).replace(/%20/g, ",");
        } else {
            parameters.city = address.City;
            parameters.state = address.State;
            parameters.country = address.Country;

            if (bStreet) {
                parameters.street = address.Address1 + ' ' + address.Address2;
                parameters.postalcode = address.Zip;
            }
        }

        if (address.MapLat && address.MapLong) {
            var location = L.latLng(address.MapLat, address.MapLong);
            addMarker(map, address, location);
            return;
        }

        //nominatim limit use is 1 request per second https://operations.osmfoundation.org/policies/nominatim/
        //we just always wait 1 sec before calling their service
        setTimeout(function (){
            var geocoderUrl = protocol + '//nominatim.openstreetmap.org/search' +
                L.Util.getParamString(parameters);

            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function() {
                if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    var response = eval("(" + xmlHttp.responseText + ")");

                    if (response.length) {
                        var location = L.latLng(response[0].lat, response[0].lon);
                        addMarker(map, address, location);
                    } else if (typeof address === 'string' && address.length) {
                        markAddress(map, address.substr(address.indexOf(' ') + 1), true);
                    } else if (bStreet) {
                        markAddress(map, address, false);
                    } else {
                        console.log(['Can\'t find address geocode.', address]);
                    }
                }
            }
            xmlHttp.open("GET", geocoderUrl, true);
            xmlHttp.send(null);
        }, 1000);
    }

    function addMarker(map, address, location) {
        console.log(location);
        var marker = L.marker(location).addTo(map);

        if (address.IsPrimary) {
            marker.bindPopup(getInfoWindowContent(address)).openPopup();
            map.setView([location.lat, location.lng], SMB.maps.zoom);
            hasPrimary = true;
        } else {
            marker.bindPopup(getInfoWindowContent(address));
        }

        if (!hasPrimary) {
            map.setView([location.lat, location.lng], SMB.maps.zoom);
        }
    }

    var hasPrimary = false;

    var map = L.map(SMB.maps.containerId).setView([0, 0], SMB.maps.zoom);
    var mapsUrl = protocol + '//smbmaps.ibsrv.net/world_tiles/{z}/{x}/{y}.png';
    L.tileLayer(mapsUrl, {
        attribution: 'Map data provided by <a href="http://www.internetbrands.com/">Internet Brands</a>',
        maxZoom: 18
    }).addTo(map);

    SMB.maps.addresses.forEach(function(address) {
        markAddress(map, address, true);
    }, this);
})(window.SMB);